// alert(`s23 discussion`)

// Objects
	// used to represent real world objects
	// information in objects are stored in "key: value" pair

// Create object using object initializers / literal notation
/*
	let objectName = {
		keyA: valueA,
		keyB: valueB
	}
*/
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};
console.log(`Object created through initializer:`)
console.log(cellphone);
console.log(typeof cellphone);

// Create object using constructor function
/*
	Syntax:
		function objectName(keyA, keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		};
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
};

let laptop = new Laptop(`Lenovo`, 2008);
console.log(`Object through constructor function:`)
console.log(laptop);

let myLaptop = new Laptop(`Macbook Air`, 2020);
console.log(`Object through constructor function again:`)
console.log(myLaptop);

let oldLaptop = Laptop(`Portal R2E CCMC`, 1980);
console.log(`Object through contructor function without new keyword:`);
console.log(oldLaptop);
// undefined

Laptop(`Portal R2E CCMC`, 1980);
// no result from plain invocation

// Creating empty objects
let computer = {}; // literal
let myComputer = new Object(); // through constructor

// Accessing object properties
	// can be used with regular concat or placeholder

//  using dot notation
console.log(`Result from dot notation: ${myLaptop.name}`);
console.log('Result from dot notation: '+ myLaptop.name);

// using square bracket notation
console.log(`Result from bracket notation: ${myLaptop[`name`]}`);

console.log(`Result from dot notation with non-existing property: ${myLaptop.specs}`);
// result: undefined

// Accessing array objects

let array = [laptop, myLaptop];

// bracket notation
console.log(`Result from bracket notation: ${array[0]['name']}`);

// dot notation
console.log(`Result from dot notation: ${array[1].manufactureDate}`);

/*
	Mini-Activity
		>> Create an object with the following key:value pair

			firstName: string
			lastName: string
			password: string
			email: string
			age: number

		>> Access each property's values
			>> 5 console.log showing all values of the object

		>> Send your output in hangouts
*/

function Person(firstName, lastName, password, email, age){
	this.FirstName = firstName;
	this.LastName = lastName;
	this.Password = password;
	this.Email = email;
	this.Age = age;
};

let personNew = new Person('John', 'Doe', '1234', 'johndoe@gmail.com', 45)
console.log(personNew.FirstName)
console.log(personNew.LastName)
console.log(personNew.Password)
console.log(personNew.Email)
console.log(personNew.Age)

// Initialize / add object properties using dot notation

let car = {};

car.name = 'Honda Civic';
console.log('Result from adding property through dot notation: ' + car);
console.log(car);

// using bracket notation
car['manufacture date'] = 2019
console.log(`Result from adding property through bracket notation: ${car}`);
console.log(car);

// deleting object properties
delete car['manufacture date'];
console.log(`Result from deleting:`);
console.log(car)

// re-assigning object properties
car.name = `Dodge Charger R/T`;
console.log(`Result from reassignement:`);
console.log(car);

// Object Methods

let person1 = {
	name: 'John',
	talk: function(){
		console.log(`Hello my name is ${this.name}`);
	}
};
console.log(person1);
console.log('Result from object method:');
person1.talk();

// adding methods to our objects
person1.walk = function(){
	console.log(`${this.name} walked 25 steps forward`);
};
person1.walk();

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: 'Austin',
		country: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function(){
		console.log(`Hello my name is ${this.firstName} ${this.lastName}. I live in ${this.address.city} and my personal email is ${this.emails[0]}.`)
	}
};
friend.introduce();

// Real world application of objects

function Pokemon(name, level){

	// Properties
	this.name = name;
	this.lvl = level;
	this.health = 2 * level;
	this.atk = level;

	// Methods
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`targetPokemon's health is now reduced to _targetPokemonHealth_`)
	}

	this.faint = function(){
		console.log(`${this.name} fainted`)
	};
};

let bulbasaur = new Pokemon("bulbasau", 16);
let rattata = new Pokemon("rattata", 8);

console.log(bulbasaur);
console.log(rattata);

bulbasaur.tackle(rattata);
rattata.tackle(bulbasaur);
bulbasaur.faint();